package model

type Member struct {
	Name   string                `form:"name" binding:"required,min=3"`
	Gender string                `form:"gender"`
	Email  string                `form:"email" binding:"required,email"`
	Age    int                   `form:"age" binding:"min=1"`
}

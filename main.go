package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/sanity-io/litter"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "gin-form-file/docs"
	"gin-form-file/pkg/model"
)

const (
	UploadDirName = "./upload"
)

func init() {
	if err := os.MkdirAll(UploadDirName, 0755); err != nil {
		log.Fatal(err)
	}
}

// @title Form file upload API
// @version 1.0
// @description Form file upload
func main() {
	r := gin.Default()
	r.LoadHTMLGlob("./public/template/*.html")

	api := r.Group("/api")
	api.GET("/livez", checkHealth)

	// put file
	// content-type application/octet-stream
	api.PUT("/file/:name", putFile)

	// form data without file
	// shoud use content-type application/x-www-form-urlencoded
	// form data with file
	// shoud use content-type multipart/form-data
	api.POST("/member", createMember)

	// static file html serve
	httpFS := gin.Dir("./public", false)
	fileServer := http.FileServer(httpFS)
	r.NoRoute(gin.WrapH(fileServer)) // convert http.handler to gin.handlerFunc

	// swagger docs
	r.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	r.Run(":3000")
}

// @Summary create member and upload photo
// @Description create member data and upload user photo
// @Tags member
// @Accept multipart/form-data
// @Produce text/html
// @Param name formData string true "memberinfo"
// @Param age formData int true "age"
// @Param gender formData string true "gender"
// @Param email formData string true "email"
// @Param file formData	file false "photo"
// @Success 200 {string} string "post success"
// @Success 400 {string} string "input invalid"
// @Success 500 {string} string "internal server error"
// @Router /api/member [post]
func createMember(c *gin.Context) {
	member := model.Member{}

	if err := c.ShouldBind(&member); err != nil {
		if validateErros, ok := err.(validator.ValidationErrors); ok {
			for _, fieldError := range validateErros {
				c.HTML(http.StatusBadRequest, "redirect.html", gin.H{
					"message":  fieldError.Error(),
					"location": "/register.html",
				})
				return
			}
		}
		c.HTML(http.StatusBadRequest, "redirect.html", gin.H{
			"message":  err,
			"location": "/register.html",
		})
		return
	}

	/*
		use c.FormFile for optional upload file
		if file is required, it can define in mode ex: `form:"file" binding:"required"`
	*/
	fileHandler, _ := c.FormFile("file")
	if fileHandler != nil { // have upload file
		from, err := fileHandler.Open()
		if err != nil {
			c.HTML(http.StatusInternalServerError, "redirect.html", gin.H{
				"message":  fmt.Sprintf("file handle error: %s", err),
				"location": "/register.html",
			})
			return
		}
		defer from.Close()

		//f, err := os.CreateTemp("./upload/", "upload-")
		nowEpoch := time.Now().Unix()
		f, err := os.Create(
			fmt.Sprintf("./upload/%d-%s",
				nowEpoch,
				strings.ReplaceAll(fileHandler.Filename, " ", "_"),
			),
		)
		if err != nil {
			c.HTML(http.StatusInternalServerError, "redirect.html", gin.H{
				"message":  fmt.Sprintf("ceate tmp error: %s", err),
				"location": "/register.html",
			})
			return
		}
		defer f.Close()

		_, err = io.Copy(f, from)
		if err != nil {
			c.HTML(http.StatusInternalServerError, "redirect.html", gin.H{
				"message":  fmt.Sprintf("copy error: %s", err),
				"location": "/register.html",
			})
			return
		}
		// Can easy to use SaveUplaodFile which privode by Gin context
		// c.SaveUploadedFile(fileHandler, "./SAVE_PATH/FILE_NAME_YOU_WANT")
	}

	litter.Dump(member)

	c.HTML(http.StatusOK, "redirect.html", gin.H{
		"message":  "register success",
		"location": "/",
	})
}

// @Summary upload binary file
// @Description upload binary file
// @Tags file
// @Accept application/octet-stream
// @Produce text/plain
// @Param filename path string true "upload filename"
// @Success 200 {string} string "post success"
// @Success 400 {string} string "filename empty"
// @Success 500 {string} string "internal server error"
// @Router /api/file/{filename} [put]
func putFile(c *gin.Context) {
	fileName := c.Param("name")
	if fileName == "" {
		c.String(http.StatusBadRequest, "filename is empty")
		return
	}

	nowEpoch := time.Now().Unix()
	f, err := os.Create(fmt.Sprintf("%s/%d-%s", UploadDirName, nowEpoch, fileName)) // if file be created, should have to close it later.
	if err != nil {
		c.String(http.StatusInternalServerError, "Create tmp error: %s", err)
		return
	}
	defer f.Close()

	_, err = io.Copy(f, c.Request.Body) // in server side, we don't need to close body.
	if err != nil {
		c.String(http.StatusInternalServerError, "IO copy error: %s", err)
		return
	}

	c.String(http.StatusOK, "uplaoded")
}

// @Summary Check api server is still responsed
// @Description for monitor to check service is activated
// @Tags system
// @Produce text/plain
// @Success 200 {string} string "should be 'ok'"
// @Router /api/livez [get]
func checkHealth(c *gin.Context) {
	c.String(http.StatusOK, "ok")
}
